-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 22, 2013 at 07:23 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `santool`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `label` varchar(128) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `url` varchar(128) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `controller` varchar(128) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `parent_id`, `label`, `position`, `url`, `status`, `controller`, `role_id`) VALUES
(1, 0, 'uncategories', 0, 'unknown', 0, NULL, NULL),
(2, 22031989, 'Home', 1, '/site/index/', 1, 'site', NULL),
(3, 22031989, 'Posts', 2, '/post/index/', 1, 'null', 1),
(4, 22031989, 'User', 3, '/user/index/', 1, 'null', 1),
(5, 3, 'All Posts', 1, '/posts/all/', 1, 'posts', NULL),
(6, 3, 'Add New', 2, '/posts/create/', 1, 'posts', NULL),
(8, 4, 'All Users', 1, '/user/index/', 1, 'user', NULL),
(11, 4, 'Add New', 2, '/user/create/', 1, 'user', NULL),
(12, 4, 'Your Profile', 3, '/user/profile/', 1, 'user', NULL),
(14, 18, 'All', 1, '/pengaduan/all/', 1, 'pengaduan', NULL),
(15, 18, 'Pending', 2, '/pengaduan/pending/', 1, 'pengaduan', NULL),
(16, 18, 'Approved', 3, '/pengaduan/approved/', 1, 'pengaduan', NULL),
(18, 22031989, 'Pengaduan', 4, '/pengaduan/all/', 1, 'NULL', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu_role`
--

CREATE TABLE IF NOT EXISTS `menu_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `menu_role`
--

INSERT INTO `menu_role` (`id`, `menu_id`, `role_id`, `status`) VALUES
(2, 17, 3, 1),
(3, 17, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `label` text,
  `resume` longtext,
  `content` longtext,
  `type` varchar(64) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `permalink` text,
  `author_id` int(11) DEFAULT NULL,
  `author_name` varchar(128) DEFAULT NULL,
  `author_email` varchar(64) DEFAULT NULL,
  `status` char(16) DEFAULT NULL,
  `terms_id` int(11) DEFAULT NULL,
  `date_create` date DEFAULT NULL,
  `date_modified` date DEFAULT NULL,
  `tags` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22031994 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `parent_id`, `label`, `resume`, `content`, `type`, `position`, `permalink`, `author_id`, `author_name`, `author_email`, `status`, `terms_id`, `date_create`, `date_modified`, `tags`) VALUES
(22031989, 0, 'ROOT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22031992, 22031989, 'HOME', '<p>home</p>\r\n', '', '4', 1, '/site/index/', 1, 'Administrator', 'surat.ajaib@gmail.com', '1', 3, '2013-02-22', '2013-02-22', ''),
(22031993, 22031989, 'BERITA', '', '', '4', 2, '/berita/index/', 1, 'Administrator', 'surat.ajaib@gmail.com', '1', 3, '2013-02-22', '2013-02-22', '');

-- --------------------------------------------------------

--
-- Table structure for table `posts_type`
--

CREATE TABLE IF NOT EXISTS `posts_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `label` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `posts_type`
--

INSERT INTO `posts_type` (`id`, `name`, `status`, `label`) VALUES
(1, 'post', '1', 'Posts'),
(2, 'link', '1', 'Links'),
(3, 'pengaduan', '1', 'Pengaduan'),
(4, 'page', '1', 'Pages'),
(5, 'comment', '1', 'Comments');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `status`) VALUES
(1, 'admin', '1'),
(2, 'user', '1');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) DEFAULT NULL,
  `label` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1001 ;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `name`, `label`) VALUES
(1, 'approved', 'Approved'),
(2, 'pending', 'Pending'),
(3, 'publish', 'Publish'),
(4, 'unpublish', 'Unpublish'),
(5, 'draft', 'Draft'),
(6, 'trash', 'Trash'),
(7, 'accepted', 'Accepted'),
(8, 'rejected', 'Rejected'),
(9, 'processed', 'Processed'),
(1000, 'spam', 'Spam');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE IF NOT EXISTS `terms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `label` varchar(128) DEFAULT NULL,
  `content` text,
  `position` int(11) DEFAULT NULL,
  `permalink` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `parent_id`, `label`, `content`, `position`, `permalink`) VALUES
(1, 22031989, 'Uncategory', 'Uncategory', 0, NULL),
(2, 22031989, 'Berita', 'Berita', 1, ''),
(3, 22031989, 'Menu', 'menu', 3, ''),
(4, 22031989, 'Artikel', 'artikel', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `terms_relationships`
--

CREATE TABLE IF NOT EXISTS `terms_relationships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `posts_id` int(11) DEFAULT NULL,
  `terms_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `terms_relationships`
--

INSERT INTO `terms_relationships` (`id`, `posts_id`, `terms_id`) VALUES
(1, 17, 2),
(2, 19, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `password` text,
  `last_name` varchar(64) DEFAULT NULL,
  `no_paket` longtext,
  `no_ktp` longtext,
  `pekerjaan` longtext,
  `telp` longtext,
  `kota_id` int(11) DEFAULT NULL,
  `alamat` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `role_id`, `name`, `email`, `password`, `last_name`, `no_paket`, `no_ktp`, `pekerjaan`, `telp`, `kota_id`, `alamat`) VALUES
(1, 'admin', 1, 'Administrator', 'surat.ajaib@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Portal', NULL, NULL, NULL, NULL, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
